﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Net;
using HtmlAgilityPack;
using System.Threading;
using System.Linq;

namespace lab3_PR
{
    public partial class Form1 : Form
    {
        private CountdownEvent countDown;
        public List<string> obtainLinks = new List<string>();

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.button1.Enabled = false;
            richTextBox1.Clear();
            richTextBox2.Clear();

            int numVal = Int32.Parse(textBox2.Text);
            countDown = new CountdownEvent(numVal);

            for (int i = 0; i < numVal; i++)
            {
                Thread newThread = new Thread(() => parseYoutube(i));
                newThread.Start();
            }

            Thread lastThread = new Thread(() => startLastThread());
            lastThread.Start();
        }

    
    
        private void parseYoutube(int page)
        {
            int numVal = Int32.Parse(textBox2.Text);
            for (int i = 0; i < numVal; i++)
            {
                string newString = textBox1.Text;
                newString.Replace(" ", "+");
                string url = "https://www.youtube.com/results?search_query=" + newString + "&page=" + page;

                WebClient client = new WebClient();
                client.Encoding = Encoding.UTF8;

                var html = client.DownloadString(url);
                var doc = new HtmlAgilityPack.HtmlDocument();
                doc.LoadHtml(html);
                var htmlNodes = doc.DocumentNode.SelectNodes("//h3/a");

                foreach (HtmlNode node in htmlNodes)
                    {
                        this.Invoke((MethodInvoker)delegate
                        {
                            this.richTextBox1.AppendText(node.ChildNodes[0].InnerHtml + " ");
                            this.richTextBox1.AppendText("\n https://www.youtube.com" + node.Attributes["href"].Value);
                            this.richTextBox1.AppendText("\n");
                            this.obtainLinks.Add("https://www.youtube.com" + node.Attributes["href"].Value);
                            this.richTextBox1.AppendText("\n\n");
                        });
                    }
               
            }
            countDown.Signal();
        }

        private void startLastThread()
        {
            countDown.Wait();
            this.Invoke((MethodInvoker)delegate
            {
                for (int j = 0; j < obtainLinks.Count; j++)
                {

                        this.richTextBox2.AppendText("\n");
                        WebRequest req = WebRequest.Create(obtainLinks[j]);
                        WebResponse resp = req.GetResponse();
                        this.richTextBox2.AppendText(obtainLinks[j] + "\nResponse: " + ((HttpWebResponse)resp).StatusCode.ToString());
                        this.richTextBox2.AppendText("\n");
                        resp.Close();
                    
                  
                }
                this.button1.Enabled = true;
            });
        }

        
    }


}
